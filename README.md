# ToDo-app

1. ToDo LIST (only desktop website)- Technologies used (HTML5, CSS3, ReactJS, ReactHooks, Context).

2. Dependencies and npm packages used (
npx create-react-app todo
npm install react-datepicke
npm install uuid
npm i mdbootstrap
fondawesome
).

3. Running on http://localhost:3000/ :  cd  todo   =>   npm start  

4. There are four different types of react components:
    -Header.js
    -TodoList.js
    -TodoForm.js
    -Todo.js
- Context for the state - AppContext.js
- Because this is a small Application, everything is in one css file, Todo.css


5. In the list you can ADD task with (name, date and status),
you can DELETE and EDIT the tasks (Edit the name, date and status),and FILTER.
 After refresing, the data is still on the page (stored in LocalStorage).



```
cd todo
git remote add origin https://gitlab.com/k.verica/todo-app.git
git branch -M main
git push -uf origin main
```

6. Deployment - npm run build
