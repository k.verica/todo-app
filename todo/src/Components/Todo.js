import React, {useContext} from 'react'
import './Todo.css'
import { AppContext } from '../Context/AppContext'

export default function Todo({task}) {
 const  {removeTask, findTask} = useContext(AppContext)
  return (
    <div className="lists">
        <p className="name-list">{task.name}</p>
       <p>{task.date}</p>
       <p>{task.status}</p>
        <div>
           <i class="fa fa-pencil edit-icon"  aria-hidden="true" onClick={()=> findTask(task.id)}></i>
            <i class="fa fa-trash trash-icon"  aria-hidden="true" onClick={()=> removeTask(task.id)}></i>
        </div>
    </div>
  )
}
