import React, { useState, useContext, useEffect } from "react";
import "./Todo.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { AppContext } from "../Context/AppContext";

export default function TodoForm() {
  const { addTask, clearTasks, editTasks, editItem } = useContext(AppContext);

  const [name, setName] = useState("");
  const [selectDate, setSelectDate] = useState(new Date());
  const [status, setStatus] = useState("");

  const handleChange = (e) => {
    setName(e.target.value);
  };
  const handleDate = (value) => {
    setSelectDate(value);
  };
  const handleStatus = (e) => {
    const selectedStatus = e.target.value;
    setStatus(selectedStatus);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (editItem === null) {
      addTask(name, selectDate.toDateString(), status);
      setName("");
    } else {
      editTasks(name, selectDate.toDateString(), status, editItem.id);
    }
  };
  useEffect(() => {
    if (editItem !== null) {
      setName(editItem.name);
      setSelectDate(editItem.selectDate);
      setStatus(editItem.status);
    } else {
      setName("");
      setSelectDate("");
      setStatus("");
    }
  }, [editItem]);

  return (
    <form onSubmit={handleSubmit} className="form">
      <div className="forms">
        <input
          onChange={handleChange}
          value={name}
          type="text"
          className="inputs"
          placeholder="Add name.."
          required
        />
        <DatePicker
          showIcon
          selected={selectDate}
          onChange={handleDate}
          dateFormat="dd/MM/yyyy"
          className="inputs"
        />
        <select className="inputs" onChange={handleStatus}>
          <option value="">Select Status</option>
          <option value="Active">Active</option>
          <option value="Completed">Complited</option>
        </select>
      </div>
      <div className="buttons">
        <button type="submit" className="btn btns btns-a">
          {" "}
          {editItem ? "Edit" : "Add"}
        </button>
        <button type="button" className=" btn btns btns-c" onClick={clearTasks}>
          Clear
        </button>
      </div>
    </form>
  );
}
