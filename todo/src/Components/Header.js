import React from "react";
import './Todo.css'

export default function Header() {
  return (
    <>
      <h1 className="header">To Do List - <span className="planner">Your daily planner</span></h1>

    </>
  );
}
