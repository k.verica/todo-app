import React, { useContext } from "react";
import { AppContext } from "../Context/AppContext";
import Todo from "./Todo";
import "./Todo.css";

export default function TodoList() {
  const { tasks, handleFilter, filtered } = useContext(AppContext);

  console.log("tasks", tasks);
  console.log("filtered", filtered);

  return (
    <div>
      <div className="filter">
        <select className="filter-select" id="" onChange={handleFilter}>
          <option value="All">All</option>
          <option value="Active">Active</option>
          <option value="Completed">Completed</option>
        </select>
      </div>

      {tasks.length ? (
        <div className="list">
          {tasks.map((task) => {
            return <Todo task={task} key={task.id} />;
          })}
        </div>
      ) : (
        <div className="list">No Tasks..</div>
      )}
    </div>
  );
}
