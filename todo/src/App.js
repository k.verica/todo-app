import "./App.css";
import TodoList from "./Components/TodoList";
import AppContextProvider from "./Context/AppContext";
import Header from "./Components/Header";
import TodoForm from "./Components/TodoForm";

function App() {
  return (
    <AppContextProvider>
      <div className="App">
        <div className="app-container">
          <Header />
           <TodoForm/>
            <TodoList />

        </div>
      </div>
    </AppContextProvider>
  );
}

export default App;
