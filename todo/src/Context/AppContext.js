import React, { createContext, useState, useEffect } from "react";

export const AppContext = createContext();

const AppContextProvider = (props) => {
  const { v4: uuidv4 } = require("uuid");
  const initialState = JSON.parse(localStorage.getItem("tasks")) || [];

  const [tasks, setTasks] = useState(initialState);
  const [editItem, setEditItem] = useState(null);
  const [filtered, setFiltered] = useState(tasks);


  useEffect(() => {
    localStorage.setItem("tasks", JSON.stringify(tasks));
  }, [tasks]);

  const addTask = (name, date, status) => {
    setTasks([...tasks, { name, id: uuidv4(), date, status }]);
  };

  const removeTask = (id) => {
    setTasks(tasks.filter((task) => task.id !== id));
  };
  const clearTasks = () => {
    setTasks([]);
  };

  const findTask = (id) => {
    const item = tasks.find((t) => t.id === id);
    setEditItem(item);
  };

  const editTasks = (name, date, status, id) => {
    const newTasks = tasks.map((task) =>
      task.id === id ? { name, date, status, id } : task
    );
    setTasks(newTasks);
    setEditItem(null);
  };
  const handleFilter = (e) => {
    let target = e.target.value;

    if (target === "All") {
      return setFiltered(tasks);
    } else if (target === "Active") {
      const filteredTasks = tasks.filter((item) => item.status === "Active");
      return setFiltered(filteredTasks);
    } else if (target === "Completed") {
      const filteredTasks = tasks.filter((item) => item.status === "Completed");
      return setFiltered(filteredTasks);
    }
  };

  return (
    <AppContext.Provider
      value={{
        tasks,
        addTask,
        removeTask,
        clearTasks,
        findTask,
        editTasks,
        editItem,
        handleFilter,
        filtered,
      }}
    >
      {props.children}
    </AppContext.Provider>
  );
};

export default AppContextProvider;
